﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;

namespace DemoSelenium.Specs.Features
{
    public class BaseSteps
    {
        private static RemoteWebDriver _currentDriver;
        protected static RemoteWebDriver CurrentDriver
        {
            get { return _currentDriver ?? (_currentDriver = new ChromeDriver()); }
            set { _currentDriver = value; }
        }


        /// <summary>
        /// Calling this method after performing ajax call will make sure that the page is 
        /// back to normal with ajax responses.
        /// </summary>
        /// <returns></returns>
        protected bool BlockUiAppearedAndClosed()
        {
            var wait = new DefaultWait<IWebDriver>(CurrentDriver)
            {
                Timeout = TimeSpan.FromSeconds(10),
                PollingInterval = TimeSpan.FromMilliseconds(100)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            wait.Until<IWebElement>((d) => d.FindElement(By.ClassName("blockOverlay")));

            return wait.Until<bool>((d) => d.FindElements(By.ClassName("blockOverlay")).Count == 0);
        }
    }
}
