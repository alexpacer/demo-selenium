﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using DemoSelenium.Specs.Classes;
using DemoSelenium.Specs.Features.Area.Objects;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;

namespace DemoSelenium.Specs.Features.Area
{
    [Binding]
    public class FeatureASteps : BaseSteps
    {
        protected List<CompanyUser> Companies { get; set; }
        protected CompanyUser CurrentCompany { get; private set; }

        #region Before/After Feature
        [BeforeTestRun]
        protected static void SetUp()
        {
            if (CurrentDriver == null)
                CurrentDriver = new ChromeDriver();
        }

        /// <summary>
        /// Signout if possible
        /// </summary>
        [AfterScenario]
        protected static void AfterScenario()
        {
            if (CurrentDriver != null)
            {
                var logoutHref = CurrentDriver.FindElementByCssSelector("a[href^='/**MASK**/logout']");
                if (logoutHref != null)
                {
                    logoutHref.Click();
                }
            }
        }

        /// <summary>
        /// Dispose Web Driver after tests 
        /// </summary>
        [AfterTestRun]
        protected static void AfterTestRun()
        {
            if (CurrentDriver != null)
            {
                CurrentDriver.Dispose();
            }
        }
        #endregion

        #region Private methods

        private IWebElement[] FindTableItems(string selector)
        {
            var trs = CurrentDriver.FindElementsByCssSelector(selector);
            // We are filtering out first empty record message row
            return trs.Where(t => t.FindElements(By.TagName("td")).Count > 1).ToArray();
        }

        #endregion

        #region Background
        [Given(@"I assume following companies exists")]
        public void GivenIAssumeFollowingCompaniesExists(Table table)
        {
            Companies = table.Rows.Select(r => new CompanyUser
            {
                Company = r["Company"],
                DefaultUser = r["Default User"],
                Token = r["Token"],
                Password = r["Password"]
            }).ToList();
        }
        #endregion

        #region Common Steps
        [Given(@"I visit the site and login as ""(.*)""'s user")]
        public void GivenIVisitTheSiteAndLoginAsAsSUser(string companyName)
        {
            this.CurrentCompany = Companies
                .First(x =>
                       x.Company.ToLower(CultureInfo.InvariantCulture).Trim() ==
                       companyName.ToLower(CultureInfo.InvariantCulture).Trim());

            var url = string.Format("{0}/?t={1}",
                Settings.CurrentSettings.Area.Url, this.CurrentCompany.Token);
            CurrentDriver.Navigate().GoToUrl(url);

            var userNameField = CurrentDriver.FindElementsByName("username")[0];
            userNameField.Clear();
            userNameField.SendKeys(this.CurrentCompany.DefaultUser);
            CurrentDriver.FindElementsByName("password")[0].SendKeys(this.CurrentCompany.Password);
            CurrentDriver.FindElementsById("login")[0].Click();
        }

        [Scope(Feature = "FeatureA")]
        [When(@"I select ""(.*)"" in year dropdown")]
        public void WhenISelectInReportCycleDropdown(string reportCycleSelection)
        {
            var reportCycleSel = CurrentDriver.FindElementByCssSelector("select[data-js='year']");
            Assert.IsNotNull(reportCycleSel);

            // Select year dropdown
            new SelectElement(reportCycleSel).SelectByText(reportCycleSelection);
            BlockUiAppearedAndClosed();
        }

        [Scope(Feature = "FeatureA")]
        [When(@"I visit report page")]
        public void WhenIVisitReportPage()
        {
            var url = Settings.CurrentSettings.Area.ReportUrl;

            CurrentDriver.Navigate().GoToUrl(url);
        }

        [Scope(Feature = "FeatureA")]
        [When(@"I click report of ""(.*)"" link with ""(.*)"" as status")]
        public void WhenIClickReportOfLinkWithAsStatus(string reportCycle, string status)
        {
            var items = this.FindTableItems("#list > tbody > tr");
            var link = items.First(tr =>
                                   tr.FindElements(By.TagName("td"))[0].Text.Contains(reportCycle) &&
                                   tr.FindElements(By.TagName("td"))[1].Text.Contains(status))
                            .FindElement(By.TagName("a"));
            link.Click();
        }


        [Scope(Feature = "FeatureA")]
        [Then(@"I should see my company name ""(.*)"" for the report")]
        public void ThenIShouldSeeMyCompanyNameAboveTheReport(string companyName)
        {
            var cNameElement = CurrentDriver.FindElement(By.ClassName("company_name"));
            Assert.IsNotNull(cNameElement);
            Assert.AreEqual(companyName, cNameElement.Text);
        }



        
        #endregion

        #region EMEA_001
        [Scope(Tag = "@Scenario1")]
        [Then(@"I should see ""(.*)"" displayed on page")]
        public void ThenIShouldSeeDisplayedOnPage(string message)
        {

            var reports = CurrentDriver.FindElementsByCssSelector("#list > tbody > tr");

            Assert.AreEqual(message, reports.First().Text);
            Assert.AreNotEqual(0, reports.Count);
        }

        #endregion

        #region EMEA_002

        [Scope(Tag = "@Scenario2")]
        [Then(@"I should see all reports' created date are in year ""(.*)""")]
        public void ThenIShouldSeeAllReportsCreateDateAreInYear(string year)
        {
            var trs = CurrentDriver.FindElementsByCssSelector("#list > tbody > tr");
            // We are filtering out first empty record message row
            foreach (var tr in trs.Where(t=>t.FindElements(By.TagName("td")).Count > 1)) 
            {
                var tdReportCycel = tr.FindElements(By.TagName("td"))[0].FindElement(By.TagName("a")).Text.Trim();
                Assert.AreEqual(true, tdReportCycel.Contains(year));
            }
        }

        [Scope(Tag = "@Scenario2")]
        [Then(@"I should expect report status of ""(.*)"" to have empty Submission date")]
        public void ThenIShouldExpectReportStatusOfToHaveEmptySubmissionDate(string status)
        {
            var trs = CurrentDriver.FindElementsByCssSelector("#list > tbody > tr");
            foreach (var tr in trs.Where(t => t.FindElements(By.TagName("td")).Count > 1))
            {
                var tdStatus = tr.FindElements(By.TagName("td"))[1].Text.Trim();
                var tdSubmissionDate = tr.FindElements(By.TagName("td"))[2].Text.Trim();
                if (tdStatus == status)
                    Assert.IsEmpty(tdSubmissionDate);
            }
        }

        #endregion

        #region EMEA_003
        [Scope(Tag = @"@Scenario3")]
        [Then(@"page footer should display ""(.*)"" and its icon ""(.*)""")]
        public void ThenPageFooterShouldDisplayAndItsIcon(string message, string imagePath)
        {
            var infobar = CurrentDriver.FindElement(By.ClassName("ib"));
            var containsMessage = infobar.FindElements(By.TagName("li")).Any(li => li.Text.Contains(message.Trim()));
            var img = infobar.FindElements(By.TagName("li")).Any(li=>li.FindElement(By.TagName("img")).GetAttribute("src").Contains(imagePath.Trim()));

            Assert.IsTrue(img);
            Assert.IsTrue(containsMessage);
        }

        [Scope(Tag = @"@Scenario3")]
        [Then(@"I should see all report's ""(.*)"" column contains value")]
        public void ThenIShouldSeeAllReportSColumnContainsValue(string columnName)
        {
            var trs = CurrentDriver.FindElementsByCssSelector("#list > tbody > tr");
            // We are filtering out first empty record message row
            foreach (var tr in trs.Where(t => t.FindElements(By.TagName("td")).Count > 1))
            {
                var tdSubmissionDate = tr.FindElements(By.TagName("td"))[2].Text.Trim();
                Assert.IsNotNullOrEmpty(tdSubmissionDate);
            }
        }

        [Scope(Tag = "@Scenario3")]
        [Then(@"I should see ""(.*)"" column ""(.*)"" highlighted and ""(.*)"" icon ""(.*)"" is ""(.*)"" if the value date is ""(.*)"" then ""(.*)"" days before")]
        public void ThenIShouldSeeColumnHighlightedAndIconIsShownIfTheValueDateIsFurtureThenCurrentDate(
            string columnName, string isOrNotHighlighted, string iconLabel, string iconImage, string iconShownOrHidden, string dateFurtureOrLater, int daysBefore)
        {
            var trs = CurrentDriver.FindElementsByCssSelector("#list > tbody > tr");
            // We are filtering out first empty record message row
            foreach (var tr in trs.Where(t => t.FindElements(By.TagName("td")).Count > 1))
            {
                var tdCreatedOn = tr.FindElements(By.TagName("td"))[2];
                Assert.IsNotNullOrEmpty(tdCreatedOn.Text.Trim());

                DateTime createdOnDate;

                Assert.IsTrue(DateTime.TryParse(tdCreatedOn.FindElements(By.TagName("span"))[0].Text, out createdOnDate));

                // column is highlighted
                if (isOrNotHighlighted.ToLower().Trim() == "is" && (DateTime.Now.AddDays(-daysBefore).CompareTo(createdOnDate) > 0)) // older
                    Assert.IsTrue(tdCreatedOn.GetAttribute("class").Contains("warning"));
                else if (isOrNotHighlighted.ToLower().Trim() == "not" && (DateTime.Now.AddDays(-daysBefore).CompareTo(createdOnDate) < 0)) // furture
                    Assert.IsFalse(tdCreatedOn.GetAttribute("class").Contains("warning"));

                // -------------

                var icon = tdCreatedOn.FindElement(By.TagName("img"));
                    Assert.IsNotNull(icon);
                // Icon is shown/hidden
                    if (iconShownOrHidden.ToLower().Trim() == "shown" && (DateTime.Now.AddDays(-daysBefore).CompareTo(createdOnDate) > 0)) // older
                {
                    Assert.IsTrue(icon.Displayed);
                    Assert.AreEqual(iconLabel, icon.GetAttribute("data-title"));
                    Assert.AreEqual(iconLabel, icon.GetAttribute("title"));
                    Assert.IsTrue(icon.GetAttribute("src").Contains(iconImage));
                }
                    else if (iconShownOrHidden.ToLower().Trim() == "hidden" && (DateTime.Now.AddDays(-daysBefore).CompareTo(createdOnDate) < 0)) // furtuer
                {
                    Assert.IsFalse(icon.Displayed);
                }


            }
        }

        #endregion

        #region EMEA_004

        [Scope(Tag = "@Scenario4")]
        [Then(@"I should see ""(.*)"" for the report's status")]
        public void ThenIShouldSeeMyStatusForReportStatus(string pstatus)
        {
            var statusSpan = CurrentDriver.FindElement(By.CssSelector("h6[class='subtitle']")).FindElement(By.TagName("span"));
            Assert.IsNotNull(statusSpan);
            Assert.AreEqual(pstatus, statusSpan.Text);
        }

        [Scope(Tag = "@Scenario4")]
        [Then(@"I should see ""(.*)"" as the report title")]
        public void ThenIShouldSeeAsTheReportTitle(string reportCycle)
        {
            var middleArea = CurrentDriver.FindElement(By.ClassName("mid"));
            var lblSiteMap = middleArea.FindElement(By.ClassName("site"));
            var text = lblSiteMap.FindElement(By.TagName("span")).Text;

            Assert.IsNotNull(lblSiteMap);
            Assert.AreEqual(reportCycle, text);
        }

        [Scope(Tag = "@Scenario4")]
        [Then(@"I should see items ordered by ""(.*)"" then ""(.*)""")]
        public void ThenIShouldSeeItemsOrderedByThen(string firstColumn, string nextColumn)
        {
            var rows = this.FindTableItems("#detail_table > tbody > tr");
            var orderedRows =
                rows.OrderBy(x => x.FindElements(By.TagName("td"))[0].Text)
                    .ThenBy(x => x.FindElements(By.TagName("td"))[1].Text)
                    .ToArray();
            // verify ordering
            // Name[0] > Units[1]

            for (var i = 0; i < orderedRows.Length; i++)
            {
                var name = orderedRows[i].FindElements(By.TagName("td"))[0];
                var units = orderedRows[i].FindElements(By.TagName("td"))[1];
                Assert.AreEqual(name.Text, rows[i].FindElements(By.TagName("td"))[0].Text);
                Assert.AreEqual(units.Text, rows[i].FindElements(By.TagName("td"))[1].Text);
            }
        }

        [Scope(Tag = "@Scenario4")]
        [Then(@"I should see records ordered by ""(.*)"" field in descending order")]
        public void ThenIShouldSeeRecordsOrderedByFieldInDescendingOrder(string columnName)
        {
            var histories = CurrentDriver.FindElements(By.ClassName("datatable")).Last().FindElements(By.CssSelector("tbody > tr")).ToList();
            var orderedHistories = histories.OrderByDescending(r => DateTime.Parse(r.FindElement(By.TagName("td")).Text)).ToList();

            for (var i = 0; i < orderedHistories.Count(); i++)
            {
                Assert.AreEqual(orderedHistories[i].FindElements(By.TagName("td"))[0].Text, histories[i].FindElements(By.TagName("td"))[0].Text);
                Assert.AreEqual(orderedHistories[i].FindElements(By.TagName("td"))[1].Text, histories[i].FindElements(By.TagName("td"))[1].Text);
                Assert.AreEqual(orderedHistories[i].FindElements(By.TagName("td"))[2].Text, histories[i].FindElements(By.TagName("td"))[2].Text);
                Assert.AreEqual(orderedHistories[i].FindElements(By.TagName("td"))[3].Text, histories[i].FindElements(By.TagName("td"))[3].Text);
            }
        }

        [Scope(Tag = "@Scenario4")]
        [Then(@"I should see ""(.*)"" button displayed on page")]
        public void ThenIShouldSeeButtonDisplayedOnPage(string btnName)
        {
            Assert.IsTrue(CurrentDriver.FindElements(By.CssSelector("input[type='button']")).Any(b=>b.GetAttribute("value") == btnName));
        }

        [Scope(Tag = "@Scenario4")]
        [Then(@"I ""(.*)"" see ""(.*)"" message with due day ""(.*)""")]
        public void ThenIShouldSeeMessageWithDueDay(string op, string message, string date)
        {
            var note = CurrentDriver.FindElementByClassName("note");

            if (op.ToLower().Trim() == "should")
            {
                Assert.IsTrue(note.Text.Contains(message));
                Assert.IsTrue(note.Displayed);
                Assert.IsTrue(note.FindElement(By.TagName("span")).Text.Contains(date));
            }
            else
            {
                Assert.IsFalse(note.Text.Contains(message));
                Assert.IsFalse(note.FindElement(By.TagName("span")).Text.Contains(date));
            }
        }

        #endregion
    }
}
