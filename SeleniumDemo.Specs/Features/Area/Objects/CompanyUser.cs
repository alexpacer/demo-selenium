﻿namespace DemoSelenium.Specs.Features.Area.Objects
{
    public class CompanyUser
    {
        public string Company { get; set; }
        public string Token { get; set; }
        public string DefaultUser { get; set; }
        public string Password { get; set; }
    }
}
