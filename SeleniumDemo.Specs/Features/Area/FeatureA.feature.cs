﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:1.9.0.77
//      SpecFlow Generator Version:1.9.0.0
//      Runtime Version:4.0.30319.18052
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace DemoSelenium.Specs.Features.Area
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "1.9.0.77")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("FeatureA")]
    public partial class FeatureAFeature
    {
        
        private static TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "FeatureA.feature"
#line hidden
        
        [NUnit.Framework.TestFixtureSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "FeatureA", "In order to avoid silly UI mistakes\r\nAs an User\r\nI want to be able to go through " +
                    "my reports and verify components on UI", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.TestFixtureTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioSetup(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioStart(scenarioInfo);
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void FeatureBackground()
        {
#line 6
 #line hidden
            TechTalk.SpecFlow.Table table1 = new TechTalk.SpecFlow.Table(new string[] {
                        "Company",
                        "Token",
                        "Default User",
                        "Password"});
            table1.AddRow(new string[] {
                        "COMPANY_A",
                        "TOKEN_A",
                        "user_a",
                        "password"});
            table1.AddRow(new string[] {
                        "COMPANY_B",
                        "TOKEN_B",
                        "user_b",
                        "password"});
            table1.AddRow(new string[] {
                        "COMPANY_C",
                        "TOKEN_C",
                        "user_c",
                        "password"});
#line 7
  testRunner.Given("I assume following companies exists", ((string)(null)), table1, "Given ");
#line hidden
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Scenario1")]
        [NUnit.Framework.CategoryAttribute("Scenario1")]
        public virtual void Scenario1()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Scenario1", new string[] {
                        "Scenario1"});
#line 14
this.ScenarioSetup(scenarioInfo);
#line 6
 this.FeatureBackground();
#line 15
 testRunner.Given("I visit the site and login as \"COMPANY_A\"\'s user", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 16
 testRunner.Then("I should see \"no report generated.\" displayed on page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Scenario2")]
        [NUnit.Framework.CategoryAttribute("Scenario2")]
        public virtual void Scenario2()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Scenario2", new string[] {
                        "Scenario2"});
#line 19
this.ScenarioSetup(scenarioInfo);
#line 6
 this.FeatureBackground();
#line 20
 testRunner.Given("I visit the site and login as \"COMPANY_B\"\'s user", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 21
 testRunner.When("I select \"2014\" in year dropdown", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 22
 testRunner.Then("I should see all reports\' created date are in year \"2014\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 23
 testRunner.Then("I should expect report status of \"Emailed\" to have empty Submission date", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Scenario3")]
        [NUnit.Framework.CategoryAttribute("Scenario3")]
        public virtual void Scenario3()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Scenario3", new string[] {
                        "Scenario3"});
#line 26
this.ScenarioSetup(scenarioInfo);
#line 6
 this.FeatureBackground();
#line 27
 testRunner.Given("I visit the site and login as \"COMPANY_C\"\'s user", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 28
 testRunner.When("I visit report page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 29
 testRunner.Then("page footer should display \"Refreshed\" and its icon \"/images/icon_refreshed.png\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 30
 testRunner.Then("page footer should display \"New item\" and its icon \"/images/icon_new-item.png\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 31
 testRunner.Then("I should see all report\'s \"Created on\" column contains value", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 32
 testRunner.Then("I should see \"Created on\" column \"is\" highlighted and \"New item\" icon \"/images/ic" +
                    "on_new-item.png\" is \"shown\" if the value date is \"furture\" then \"2\" days before", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 33
 testRunner.When("I select \"2013\" in year dropdown", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 34
 testRunner.Then("I should see \"Created on\" column \"not\" highlighted and \"Refreshed\" icon \"/images/" +
                    "mxcpu103.png\" is \"hidden\" if the value date is \"older\" then \"2\" days before", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Scenario41")]
        [NUnit.Framework.CategoryAttribute("Scenario4")]
        public virtual void Scenario41()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Scenario41", new string[] {
                        "Scenario4"});
#line 37
this.ScenarioSetup(scenarioInfo);
#line 6
 this.FeatureBackground();
#line 38
 testRunner.Given("I visit the site and login as \"COMPANY_C\"\'s user", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 39
 testRunner.When("I visit report page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 40
 testRunner.When("I select \"2014\" in year dropdown", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 41
 testRunner.And("I click report of \"Report A\" link with \"Archived\" as status", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 42
 testRunner.Then("I should see my company name \"COMPANY_C\" for the report", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 43
 testRunner.Then("I should see \"Archived\" for the report\'s status", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 44
 testRunner.Then("I should see \"Report A\" as the report title", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 45
    testRunner.Then("I should see items ordered by \"Name\" then \"Created\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 46
 testRunner.Then("I should see records ordered by \"Created on\" field in descending order", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 47
 testRunner.Then("I should see \"Email\" button displayed on page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 48
 testRunner.Then("I should see \"Cancel\" button displayed on page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 49
 testRunner.Then("I should see \"Save Draft\" button displayed on page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 50
 testRunner.Then("I \"should\" see \"Report will be emailed on \" message with due day \"2014-09-25\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Scenario42")]
        [NUnit.Framework.CategoryAttribute("Scenario4")]
        public virtual void Scenario42()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Scenario42", new string[] {
                        "Scenario4"});
#line 53
this.ScenarioSetup(scenarioInfo);
#line 6
 this.FeatureBackground();
#line 54
 testRunner.Given("I visit the site and login as \"COMPANY_B\"\'s user", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 55
 testRunner.When("I visit report page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 56
 testRunner.When("I select \"2014\" in year dropdown", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 57
 testRunner.And("I click report of \"Report B\" link with \"Archived\" as status", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 58
 testRunner.Then("I should see my company name \"COMPANY_B\" for the report", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 59
 testRunner.Then("I should see \"Archived\" for the report\'s status", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 60
 testRunner.Then("I should see \"Report B\" as the report title", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 61
    testRunner.Then("I should see items ordered by \"Name\" then \"Created\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 62
 testRunner.Then("I should see records ordered by \"Createdd on\" field in descending order", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 63
 testRunner.Then("I should see \"Email\" button displayed on page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 64
 testRunner.Then("I should see \"Cancel\" button displayed on page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 65
 testRunner.Then("I should see \"Save\" button displayed on page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 66
 testRunner.Then("I \"should not\" see \"Report will be emailed on \" message with due day \"2014-09-25\"" +
                    "", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
