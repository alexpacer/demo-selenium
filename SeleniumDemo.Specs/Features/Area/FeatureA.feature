﻿Feature: FeatureA
	In order to avoid silly UI mistakes
	As an User
	I want to be able to go through my reports and verify components on UI

	Background: Existing Companies
		Given I assume following companies exists
		| Company   | Token   | Default User | Password |
		| COMPANY_A | TOKEN_A | user_a       | password |
		| COMPANY_B | TOKEN_B | user_b       | password |
		| COMPANY_C | TOKEN_C | user_c       | password |

@Scenario1
Scenario: Scenario1
	Given I visit the site and login as "COMPANY_A"'s user
	Then I should see "no report generated." displayed on page

@Scenario2
Scenario: Scenario2
	Given I visit the site and login as "COMPANY_B"'s user
	When I select "2014" in year dropdown
	Then I should see all reports' created date are in year "2014"
	Then I should expect report status of "Emailed" to have empty Submission date

@Scenario3
Scenario: Scenario3
	Given I visit the site and login as "COMPANY_C"'s user
	When I visit report page
	Then page footer should display "Refreshed" and its icon "/images/icon_refreshed.png"
	Then page footer should display "New item" and its icon "/images/icon_new-item.png"
	Then I should see all report's "Created on" column contains value
	Then I should see "Created on" column "is" highlighted and "New item" icon "/images/icon_new-item.png" is "shown" if the value date is "furture" then "2" days before
	When I select "2013" in year dropdown
	Then I should see "Created on" column "not" highlighted and "Refreshed" icon "/images/mxcpu103.png" is "hidden" if the value date is "older" then "2" days before

@Scenario4
Scenario: Scenario41
	Given I visit the site and login as "COMPANY_C"'s user
	When I visit report page
	When I select "2014" in year dropdown
	And I click report of "Report A" link with "Archived" as status
	Then I should see my company name "COMPANY_C" for the report
	Then I should see "Archived" for the report's status
	Then I should see "Report A" as the report title
    Then I should see items ordered by "Name" then "Created"
	Then I should see records ordered by "Created on" field in descending order
	Then I should see "Email" button displayed on page
	Then I should see "Cancel" button displayed on page
	Then I should see "Save Draft" button displayed on page
	Then I "should" see "Report will be emailed on " message with due day "2014-09-25"

@Scenario4 
Scenario: Scenario42
	Given I visit the site and login as "COMPANY_B"'s user
	When I visit report page
	When I select "2014" in year dropdown
	And I click report of "Report B" link with "Archived" as status
	Then I should see my company name "COMPANY_B" for the report
	Then I should see "Archived" for the report's status
	Then I should see "Report B" as the report title
    Then I should see items ordered by "Name" then "Created"
	Then I should see records ordered by "Createdd on" field in descending order
	Then I should see "Email" button displayed on page
	Then I should see "Cancel" button displayed on page
	Then I should see "Save" button displayed on page
	Then I "should not" see "Report will be emailed on " message with due day "2014-09-25"