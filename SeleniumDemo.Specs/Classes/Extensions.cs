﻿using System.Xml.Linq;

namespace DemoSelenium.Specs.Classes
{
    public static class Extensions
    {
        public static string ValueOrEmpty(this XElement element)
        {
            return (element == null)
                       ? string.Empty
                       : element.Value;
        }
    }
}
