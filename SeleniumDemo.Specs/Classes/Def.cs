﻿namespace DemoSelenium.Specs.Classes
{

    public static class Status
    {
        public static string StatusA { get { return "StatusA"; } }
        public static string StatusB { get { return "StatusB"; } }
        public static string StatusC { get { return "StatusC"; } }
        public static string StatusD { get { return "StatusD"; } }
    }
}
