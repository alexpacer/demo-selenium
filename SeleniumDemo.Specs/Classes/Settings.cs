﻿using System;
using System.Xml.Linq;

namespace DemoSelenium.Specs.Classes
{
    public class AreaSettings
    {
        public string Url { get; set; }

        public string ReportUrl{ get; set; }
    }

    public class Settings
    {
        private Settings(string url)
        {
            BaseUrl = url;
        }
        #region Properties
        public string BaseUrl { get; private set; }
        public string Token { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public AreaSettings Area { get; private set; }

        private static Settings _settings;
        public static Settings CurrentSettings
        {
            get
            {
                if (_settings == null)
                {
                    LoadSettings("TestRun.config");
                }
                return _settings;
            }
        }

        #endregion

        /// <summary>
        /// Load XML configuration settings into properties
        /// </summary>
        /// <param name="file"></param>
        private static void LoadSettings(String file)
        {
            var xml = XElement.Load(file);
            var url = xml.Element("URL").ValueOrEmpty();

            _settings = new Settings(url)
                {
                    Username = xml.Element("username").ValueOrEmpty(),
                    Password = xml.Element("password").ValueOrEmpty(),
                    Token = xml.Element("token").ValueOrEmpty(),
                    Area = new AreaSettings
                        {
                            Url = xml.Element("Area").Element("URL").ValueOrEmpty(),
                            ReportUrl = xml.Element("Area").Element("ReportURL").ValueOrEmpty()
                        }
                };

        }


    }


}
